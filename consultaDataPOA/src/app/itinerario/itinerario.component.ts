import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Itinerario } from '../shared/models/itinerario.model';
import { LatLon } from '../shared/models/latLon.model';
import { ItinerarioService } from '../shared/service/itinerario.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

  idItinerario: number;
  itinerario = {} as Itinerario;
  latLon: Array<LatLon> = [];
  latLonM = {} as number;

  telaOrigem: any;

  // paginação
  page = 1;
  count = 0;
  tableSize = 5;
  tableSizes = [5, 15, 25, 50, 75, 100];


  constructor(
    private itinerarioService: ItinerarioService,
    private route: ActivatedRoute,
  ) {
    // para captar o id vindo de lotação ao linha
    this.route.params.subscribe(params => this.idItinerario = params['id']);
    console.log(this.idItinerario);

    // para captar de qual tela veio
    this.route.queryParams.subscribe(params => this.telaOrigem = params['tela']);
    console.log(this.telaOrigem);
    
  }


  ngOnInit(): void {
    this.getItinerario();


  }

  getItinerario() {
    this.itinerarioService.getItinerario(this.idItinerario.toString()).subscribe((itinerario: Itinerario) => {
      this.itinerario = itinerario;
      this.itinerario.idlinha = itinerario.idlinha;
      this.itinerario.codigo = itinerario.codigo;
      this.itinerario.nome = itinerario.nome;

    });

    this.itinerarioService.getLatLng(this.idItinerario.toString())
      .then((response: any) => {
        for (let i = 0; i < Object.keys(response).length - 3; i++) {
          this.latLon.push(response[i]);
        }
        this.itinerario.latLon = this.latLon;
      })
  }


  visualizarGoogleMaps(lat: number, lng: number) {
    window.open(environment.URLMAPS + lat + lng);
  }

  onTableDataChange(event) {
    this.page = event;
    this.getItinerario();
  }

  onTableSizeChange(event: { target: { value: number; }; }): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getItinerario();
  }

}
