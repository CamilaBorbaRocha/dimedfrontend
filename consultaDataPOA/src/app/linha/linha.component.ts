import { Component, OnInit } from '@angular/core';
import { LinhaOnibus } from '../shared/models/linhaOnibus.model';
import { LinhaOnibusService } from '../shared/service/linha-onibus.service';

@Component({
  selector: 'app-linha',
  templateUrl: './linha.component.html',
  styleUrls: ['./linha.component.css']
})
export class LinhaComponent implements OnInit {
  linhasDeOnibus: LinhaOnibus[] = [];

  // alterar o que será mostrado na tela
  mode = 'inicio';

  // filtro
  pesquisa: string;

  // pesquisa
  pesquisaLinha: string;


  // paginação
  page = 1;
  count = 0;
  tableSize = 5;
  tableSizes = [5, 15, 25, 50, 75, 100];

  constructor(private linhaService: LinhaOnibusService) {
  }

  ngOnInit(): void {
    this.getBuscarLinha();
  }

  getBuscarLinha() {
    this.linhaService.getLinhaOnibus().subscribe((linhasDeOnibus: LinhaOnibus[]) => {
      console.log(linhasDeOnibus);
      this.linhasDeOnibus = linhasDeOnibus;
    });
    console.log(this.linhasDeOnibus);
  }

  onTableDataChange(event) {
    this.page = event;
    this.getBuscarLinha();
  }

  onTableSizeChange(event: { target: { value: number; }; }): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getBuscarLinha();
  }

  getAvisoTamanho() {
    window.alert('Por esse seletor pode ser alterado o numero de itens que está sendo mostrado na tabela, por padrão ela inicia com 5 itens.');
  }

  changeMode(mode: string) {
    this.mode = mode;
    this.pesquisa = '';
    this.pesquisaLinha = '';
  }
}
