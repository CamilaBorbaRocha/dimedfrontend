import { Component, OnInit } from '@angular/core';
import { Lotacao } from '../shared/models/lotacao.model';
import { LotacaoService } from '../shared/service/lotacao.service';

@Component({
  selector: 'app-lotacao',
  templateUrl: './lotacao.component.html',
  styleUrls: ['./lotacao.component.css']
})
export class LotacaoComponent implements OnInit {
  lotacao: Lotacao[] = [];

  // paginação
  page = 1;
  count = 0;
  tableSize = 5;
  tableSizes = [5, 15, 25, 50, 75, 100];

  // alterar o que será mostrado na tela
  mode = 'inicio';

  // filtro
  pesquisaNome: string;

  // pesquisa
  pesquisaLotacao: string;

  constructor(private lotacaoService: LotacaoService) { }

  ngOnInit(): void {
    this.getBuscarLotacao();
  }

  getBuscarLotacao() {
    this.lotacaoService.getLotacao().subscribe((lotacao: Lotacao[]) => {
      console.log(lotacao);
      this.lotacao = lotacao;
    });

    console.log(this.lotacao);

  }

  changeMode(mode: string) {
    this.mode = mode;
    this.pesquisaNome = '';
    this.pesquisaLotacao = '';
  }

  onTableDataChange(event: number) {
    this.page = event;
    this.getBuscarLotacao();
  }

  onTableSizeChange(event: { target: { value: number; }; }): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.getBuscarLotacao();
  }

  getAvisoTamanho() {
    window.alert('Por esse seletor pode ser alterado o numero de itens que está sendo mostrado na tabela, por padrão ela inicia com 5 itens.');
  }


}
