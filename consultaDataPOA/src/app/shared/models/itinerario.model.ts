import { LatLon } from "./latLon.model";

export interface Itinerario{
    idlinha: string,
    codigo: string
    nome: string,
    latLon: Array<LatLon>;
}