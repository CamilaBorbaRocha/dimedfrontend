import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable, throwError, of } from 'rxjs';
import { LinhaOnibus } from '../models/linhaOnibus.model';
import { catchError } from 'rxjs/operators';
import { Lotacao } from '../models/lotacao.model';

@Injectable({
  providedIn: 'root'
})
export class LotacaoService {

  constructor(public http: HttpClient) { }


  getLotacao(): Observable<Lotacao[]>{
    return this.http.get<Lotacao[]>(environment.URLBASE + '?a=nc&p=%25&t=l').pipe(
      catchError(this.errorHandler<Lotacao[]>([], 'Erro ao buscar lotação.'))
    );
  }

  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }

  
}
